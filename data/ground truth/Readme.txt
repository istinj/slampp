Here, there is ground truth for some datasets.

The files ending in "_groundtruth.txt" are noise-less datasets, the ones ending in "_groundtruth_solution.txt" are the raw vertex values of the ground truth.

The ground truth for the 10k and 100k datasets [1] was created by removing the noise from the odometry and removing all the loop closures from the original datasets using the 10k_ground-truth.sh script.

For the sphere2500, city10k, cityTrees10k [3] and manhattanOlson3500 datasets [2], the ground truth is provided by the authors and can be downloaded from the repository linked by https://www.openslam.org/iSAM.html.

The cityTrees10k ground truth dataset was modified; the ids of the vertices were reassigned in order for the dataset to be sorted and the information matrices were squared. This was done using the fix_cityTrees10k_ground-truth.sh script.

The city10k and manhattanOlson3500 were not modified (although the information matrices should have been squared).

Note that the sphere2500 ground truth dataset was not modified in any way (while the information matrices in the sphere2500 in datasets folder were converted from RPY to axis angle).

[1] G. Grisetti, C. Stachniss, S. Grzonka, and W. Burgard, "A tree parameterization for efficiently computing maximum likelihood maps using gradient descent," in Robotics: Science and Systems (RSS), June 2007.
[2] E. Olson, "Robust and efficient robot mapping," Ph.D. dissertation, Massachusetts Institute of Technology, 2008.
[3] M. Kaess, A. Ranganathan, and F. Dellaert, "iSAM: Fast incremental smoothing and mapping with efficient data association," in IEEE Intl. Conf. on Robotics and Automation (ICRA), Rome, Italy, April 2007, pp. 1670-1677.
