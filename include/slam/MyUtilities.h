#pragma once
#include <chrono>
#include <fstream>
#include <iostream>
#include <sys/time.h>
#include <vector>

namespace internal_utils {
  inline double tv2sec(const struct timeval& tv_) {
    return (static_cast<double>(tv_.tv_sec) + 1e-6 * static_cast<double>(tv_.tv_usec));
  }

  double getTime();

  class TicToc {
  public:
    //! @brief start measuring time when called (NOT THREAD SAFE)
    static void tic() {
      _time_seconds_last_tic = getTime();
    };

    //! @brief compute time passed since the last tic() call in seconds (NOT THREAD SAFE)
    static double toc() {
      return (getTime() - _time_seconds_last_tic);
    };

  protected:
    //! @brief time of last tic call (timing) (NOT THREAD SAFE)
    static double _time_seconds_last_tic;
  };

  struct MyTimings {
    // ia my time
    double t_iteration     = 0.f; // ia total time for iteration (one Optimize)
    double t_update_lambda = 0.f; // ia time to compute J + H
    double t_update_rhs    = 0.f; // ia time to compute b
    double t_linerize      = 0.f; // ia time to compute J + H + b
    double t_solve         = 0.f; // ia time to solve the linear system
    double t_update_values = 0.f; // ia time to do X <- X + Deltax
  };

  struct MyStatistics {
    int64_t it               = -1;   // ia iteration number
    double chi2_denormalized = 0.0f; // ia denormalized chi2
    double chi2_normalized   = 0.0f; // ia normalized chi2 [by (number of edges - degree of freedoms)]
    MyTimings timings;               // ia all the timings
  };

  typedef std::vector<MyStatistics> MyStatisticsVector;
  typedef std::vector<MyTimings> MyTimingsVector;

} // namespace internal_utils

std::ostream& operator<<(std::ostream& stream_, const internal_utils::MyTimings& timings_);
std::ostream& operator<<(std::ostream& stream_, const internal_utils::MyStatistics& stat_);