#include <iostream>
#ifdef _OPENMP
#include <omp.h>
#endif // _OPENMP
#include "slam/LinearSolver_UberBlock.h"
#include "slam/LinearSolver_Schur.h"
#include "slam/LinearSolver_CholMod.h"
#include "slam/LinearSolver_CSparse.h"
#include "slam/ConfigSolvers.h" // nonlinear graph solvers

#include "G2OReader.h"

const std::string exe_name = "g2o_optimization_app";
#define LOG std::cerr << exe_name + "|"

int main(const int argc, const char** argv) {
  if (argc < 5) {
    LOG << "required 3 arguments: <exec_name> <input_file.g2o> <output_file.g2o> <num_iterations> <solver>\n";
    LOG << "solver type can be: \n\t- cholmod \n\t- huber\n\t- csparse\n";
    throw std::runtime_error(exe_name + "|exit");
  }

  const std::string input(argv[1]);
  const std::string output(argv[2]);
  const int iterations = std::atoi(argv[3]);
  const std::string linear_solver_type(argv[4]);

  //ia system for pgo
  typedef g2o_bridge::G2OConverter::CSystem_SE3PGO CSystemTypeSE3PGO;
  
  //ia linear solver model based on the native implementation (fixed block size)
  typedef CLinearSolver_UberBlock<CSystemTypeSE3PGO::_TyHessianMatrixBlockList>
    CLinearSolverType_HuberBlock;
  //ia cholmod linear solver
  typedef CLinearSolver_CholMod CLinearSolverType_Cholmod;
  //ia csparse linear solver
  typedef CLinearSolver_CSparse CLinearSolverType_CSparse;


  g2o_bridge::G2OConverter::CSystem_SE3PGO system;
  
  g2o_bridge::G2OConverter converter;
  converter.loadSE3PGO(input, &system);

  //ia plot first state
  const std::string input_plot = input+"_plot.tga";
  const std::string output_plot = output+"_plot.tga";
  system.Plot3D(input_plot.c_str(), plot_quality::plot_Printing); // plot in print quality

  double initial_chi2 = 0.0f, final_chi2 = 0.0f;
  double den_initial_chi2 = 0.0f, den_final_chi2 = 0.0f;
  double total_time = 0.0f;

  if (linear_solver_type == "huber") {
    CNonlinearSolver_Lambda<CSystemTypeSE3PGO, CLinearSolverType_HuberBlock>
      solver(system);
    solver.setVerbosityDiocane(true);
    
    initial_chi2 = solver.f_Chi_Squared_Error();
    den_initial_chi2 = solver.f_Chi_Squared_Error_Denorm();

    solver.Optimize(iterations);

    final_chi2 = solver.f_Chi_Squared_Error();
    den_final_chi2 = solver.f_Chi_Squared_Error_Denorm();

    LOG << "logging time statistics\n";
    solver.Dump(total_time);
  } else if (linear_solver_type == "cholmod") {
    CNonlinearSolver_Lambda<CSystemTypeSE3PGO, CLinearSolverType_Cholmod>
      solver(system);
    solver.setVerbosityDiocane(true);
    
    initial_chi2 = solver.f_Chi_Squared_Error();
    den_initial_chi2 = solver.f_Chi_Squared_Error_Denorm();

    solver.Optimize(iterations);

    final_chi2 = solver.f_Chi_Squared_Error();
    den_final_chi2 = solver.f_Chi_Squared_Error_Denorm();

    LOG << "logging time statistics\n";
    solver.Dump(total_time);
  } else if (linear_solver_type == "csparse") {
    CNonlinearSolver_Lambda<CSystemTypeSE3PGO, CLinearSolverType_CSparse>
      solver(system);
    solver.setVerbosityDiocane(true);
    
    initial_chi2 = solver.f_Chi_Squared_Error();
    den_initial_chi2 = solver.f_Chi_Squared_Error_Denorm();

    solver.Optimize(iterations);

    final_chi2 = solver.f_Chi_Squared_Error();
    den_final_chi2 = solver.f_Chi_Squared_Error_Denorm();

    LOG << "logging time statistics\n";
    solver.Dump(total_time);
  } else {
    LOG << "invalid solver type [" << linear_solver_type << "]" << std::endl;
    LOG << "solver type can be: \n\t- cholmod \n\t- huber\n\t- csparse\n";
    throw std::runtime_error(exe_name+"|exit");
  }

  std::cerr << std::endl;

  LOG << "basic chi2 analisys" << std::endl;
  LOG << "initial chi2 = " << initial_chi2 << " | denormalized = " << den_initial_chi2 << std::endl;
  LOG << "final chi2   = " << final_chi2 << " | denormalized = " << den_final_chi2 <<  std::endl;
  LOG << "total time = " << total_time << std::endl;

  system.Plot3D(output_plot.c_str(), plot_quality::plot_Printing); // plot in print quality
  
  return 0;
  
}
