#include "G2OReader.h"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

namespace g2o_bridge {

  G2OConverter::G2OConverter() {
  }

  G2OConverter::~G2OConverter() {
  }

  void G2OConverter::loadSE3PGO(const std::string& filename_, CSystem_SE3PGO* system_ptr_) {
    const std::string log_prefix("G2OConverter::loadSE3PGO|");

    if (system_ptr_ == 0)
      throw std::runtime_error("G2OConverter::loadSE3PGO|invalid system pointer");

    if (filename_ == "")
      throw std::runtime_error("G2OConverter::loadSE3PGO|empty filename");

    // ia read the goddamn file
    std::cerr << log_prefix + "opening file [" << filename_ << "]" << std::endl;
    std::ifstream file_stream(filename_.c_str());

    size_t vertex_counter   = 0;
    size_t edge_counter     = 0;
    size_t line_counter     = 0;
    size_t skipped_elements = 0;
    std::string line;
    std::string element_tag;
    while (getline(file_stream, line)) {
      std::stringstream ss(line);

      // ia read the tag
      ss >> element_tag;

      if (element_tag == "VERTEX_SE3:QUAT") {
        // ia read the vertex
        int graph_id                  = -1;
        Vector7d vector_reading       = Vector7d::Zero();
        Vector6d vertex_minimal_state = Vector6d::Zero(); // ia t+aa

        ss >> graph_id;
        for (size_t i = 0; i < vector_reading.rows(); ++i) {
          ss >> vector_reading[i];
        }

        Matrix3d rotation = Matrix3d::Identity();
        rotation          = Eigen::Quaternion<double>(vector_reading[6], vector_reading[3], vector_reading[4], vector_reading[5]).toRotationMatrix();

        vertex_minimal_state.head(3) = vector_reading.head(3);
        vertex_minimal_state.tail(3) = C3DJacobians::v_RotMatrix_to_AxisAngle(rotation);

        // Vector3d axis_angle = Vector3d::Zero();
        // axis_angle = C3DJacobians::v_RotMatrix_to_AxisAngle(rotation);

        // ia create the vertex and add it to the system
        // CVertexPose3D vertex(vertex_minimal_state);
        system_ptr_->r_Get_Vertex<CVertexPose3D>(graph_id, vertex_minimal_state);

        ++vertex_counter;
      } else if (element_tag == "EDGE_SE3:QUAT") {
        int from_id = -1, to_id = -1;
        Vector7d vector_reading    = Vector7d::Zero();
        Vector6d edge_minimal_meas = Vector6d::Zero();
        Matrix6d information       = Matrix6d::Zero();

        // ia read graph ids
        ss >> from_id >> to_id;

        // ia read measurement
        for (size_t i = 0; i < vector_reading.rows(); ++i) {
          ss >> vector_reading[i];
        }

        // ia read information
        for (int i = 0; i < information.rows() && ss.good(); i++) {
          for (int j = i; j < information.cols() && ss.good(); j++) {
            ss >> information(i, j);
            if (i != j)
              information(j, i) = information(i, j);
          }
        }

        Matrix3d rotation = Matrix3d::Identity();
        rotation          = Eigen::Quaternion<double>(vector_reading[6], vector_reading[3], vector_reading[4], vector_reading[5]).toRotationMatrix();

        edge_minimal_meas.head(3) = vector_reading.head(3);
        edge_minimal_meas.tail(3) = C3DJacobians::v_RotMatrix_to_AxisAngle(rotation);

        // ia add the edge
        system_ptr_->r_Add_Edge(CEdgePose3D(from_id, to_id, edge_minimal_meas, information, *system_ptr_));

        ++edge_counter;
      } else {
        ++skipped_elements;
        ++line_counter;
        continue;
      }

      ++line_counter;
    }

    file_stream.close();
    std::cerr << log_prefix + "conversion ended " << std::endl;
    std::cerr << log_prefix + "read [" << line_counter << "] lines\n";
    std::cerr << log_prefix + "converted [" << vertex_counter << "] vertices and [" << edge_counter << "] edges\n";
    std::cerr << log_prefix + "skipped [" << skipped_elements << "] elements\n";
  }
} // namespace g2o_bridge
