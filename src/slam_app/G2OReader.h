#include "slam_app/Main.h"
#include "slam/SE2_Types.h" // SE(2) types
#include "slam/SE3_Types.h" // SE(3) types

namespace g2o_bridge {
  class G2OConverter {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    typedef Eigen::Matrix<double, 3, 1> Vector3d;
    typedef Eigen::Matrix<double, 6, 1> Vector6d;
    typedef Eigen::Matrix<double, 7, 1> Vector7d;
    typedef Eigen::Matrix<double, 3, 3> Matrix3d;
    typedef Eigen::Matrix<double, 6, 6> Matrix6d;

    //ia usings to avoid write poems
    // using VariableSE3 = CVertexPose3D;
    // using VariablePoint3 = CVertexLandmark3D;
    // using FactorPosePoseSE3 = CEdgePose3D;
    // using FactorPosePointSE3 = CEdgePoseLandmark3D;

    typedef MakeTypelist(CVertexPose3D) VertexPose3DList;
    typedef MakeTypelist(CEdgePose3D) EdgePose3DList;

    //ia other typedefs
    typedef CFlatSystem<CVertexPose3D, VertexPose3DList, CEdgePose3D, EdgePose3DList> CSystem_SE3PGO;
    // typedef CLinearSolver_UberBlock<CSystemType::_TyHessianMatrixBlockList> CLinearSolverType_HuberBlock;
    // typedef CLinearSolver_CholMod CLinearSolverType_Cholmod; // or cholmod

    // using SlamPP_SystemSE3PGO = CSystem_SE3PGO;
    // using SlamPP_LinearSolverHuberBlock = CLinearSolverType_HuberBlock;
    // using SlamPP_LinearSolverCholmod = CLinearSolverType_Cholmod;
    
    // CSystemType system;
    // CNonlinearSolver_Lambda<CSystemType, CLinearSolverType> solver(system);
    
    G2OConverter();
    virtual ~G2OConverter();
    
    void loadSE3PGO(const std::string& filename_, CSystem_SE3PGO* system_ptr_);
  };
}
