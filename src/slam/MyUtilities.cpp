#include "slam/MyUtilities.h"

namespace internal_utils {

  // ds timing
  double TicToc::_time_seconds_last_tic = 0.0;

  double getTime() {
    struct timeval tv;
    gettimeofday(&tv, 0);
    return tv2sec(tv);
  }
} // namespace internal_utils

std::ostream& operator<<(std::ostream& stream_, const internal_utils::MyTimings& timings_) {
  stream_ << "t_iter= " << timings_.t_iteration;
  stream_ << " t_matrix= " << timings_.t_update_lambda;
  stream_ << " t_rhs= " << timings_.t_update_rhs;
  stream_ << " t_linearize= " << timings_.t_linerize;
  stream_ << " t_solve= " << timings_.t_solve;
  stream_ << " t_update= " << timings_.t_update_values;
  return stream_;
}

std::ostream& operator<<(std::ostream& stream_, const internal_utils::MyStatistics& stat_) {
  stream_ << "it= " << stat_.it;
  stream_ << " chi2= " << stat_.chi2_denormalized;
  stream_ << " chi2_n= " << stat_.chi2_normalized;
  stream_ << " t_iter= " << stat_.timings.t_iteration;
  stream_ << " t_matrix= " << stat_.timings.t_update_lambda;
  stream_ << " t_rhs= " << stat_.timings.t_update_rhs;
  stream_ << " t_linearize= " << stat_.timings.t_linerize;
  stream_ << " t_solve= " << stat_.timings.t_solve;
  stream_ << " t_update= " << stat_.timings.t_update_values;
  return stream_;
}