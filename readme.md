## Slam++ fork
Forked version to do some tests and checkout speed and performances

###### How to build
```bash
cd <root>
mkdir build
cmake ..
make -j6 # it takes a lot of memory 12GB with 6 cores
```

###### How to run
For now on pgo, their app
```bash
cd <root>/bin
./slam_plus_plus -h
```

otherwise my app
```bash
cd <root>/bin
./g2o_optimization_app -h
```

## License
SLAM++ is originally developed by Lukas Polok & Viorela Ila & Marek Solony.
If you want to use this software, please cite the [relevant publications](https://sourceforge.net/p/slam-plus-plus/wiki/Home/) for this project.
For more information on the SLAM++ project, please visit the [official website](https://sourceforge.net/projects/slam-plus-plus/)